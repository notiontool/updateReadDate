// 参考：https://hkob.hatenablog.com/entry/2021/06/30/103000

const MY_NOTION_TOKEN = PropertiesService.getScriptProperties().getProperty("NOTION_TOKEN");
const DATABASE_ID = PropertiesService.getScriptProperties().getProperty("DATABASE_ID_WEBCLIP");
const NOTION_VERSION = PropertiesService.getScriptProperties().getProperty("NOTION_VERSION");

function main() {
  updateReadDate();
}

function updateReadDate() {
  let list = getReadDateEmptyList()["results"];
  let date_str = Utilities.formatDate(new Date(), "JST", "yyyy-MM-dd");
  let payload = {
    "properties": {
      "ReadDate": {
        "date": {
          "start": date_str
        }
      }
    }
  }
  list.forEach(t => {
    let pageId = t["id"]
    Logger.log(pageId)
    patchData(pageId, payload)
    Utilities.sleep(1000);
  })
}

function getReadDateEmptyList() {
  let payload = {
    "filter": {
      "and": [
        {
          "property": "Read",
          "checkbox": {
            "equals": true
          }
        },
        {
          "property": "ReadDate",
          "date": {
            "is_empty": true
          }
        }
      ]
    },
  }
  return fetchData(payload)
}


function fetchData(payload) {
  let url = "https://api.notion.com/v1/databases/" + DATABASE_ID + "/query"
  let options = {
    "method" : "POST",
    "headers": {
      "Content-type": "application/json",
      "Authorization": "Bearer " + MY_NOTION_TOKEN,
      "Notion-Version": NOTION_VERSION,
    },
    "payload" : JSON.stringify(payload),
    "muteHttpExceptions" : true
  }

  try {
    let res = UrlFetchApp.fetch(url, options);
    Logger.log(res);
    return JSON.parse(res);
  } catch (e) {
    Logger.log(e);
    return null;
  }
}

function patchData(pageId, payload) {
  let url = "https://api.notion.com/v1/pages/" + pageId
  let options = {
    "method" : "PATCH",
    "headers": {
      "Content-type": "application/json",
      "Authorization": "Bearer " + MY_NOTION_TOKEN,
      "Notion-Version": NOTION_VERSION,
    },
    "payload": JSON.stringify(payload)
  };
  let res = UrlFetchApp.fetch(url, options)
  Logger.log(res)
}
